import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { store, persistor } from './redux/store';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react'


ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <App />
    </PersistGate>
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

//import createStore
//store it in a const name 'store'
//create a reducer like src/store/reducer.js
//import it in the index.js file
//import Provider from the react-redux and wrap it around the <App/>
//pass the store as store={store} store is a property expected by the Provider
//import { connect } from the react-redux in the component where you want to subscribe/access the redux store
// connect is a function or you may say a HOC(Higher Order Component) 
// connect is function which returns a function and took a component as input
// usage syntax is connect(mapStateToProps, mapDispatchToProps)(Component)

