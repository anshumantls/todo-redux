import React, {Component} from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../redux/actions/todo-action-creator';

class AddToDo extends Component {
    render () {
        return (
            <div>
                <input placeholder="Enter a new task"
                value={this.props.input} 
                onChange={(event) => {this.props.changeInput(event.target.value)}}/>
                <button onClick={this.props.add} disabled={this.props.input.trim().length ? false : true}>Add Task</button>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        input : state.reducer.input
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changeInput : (value) => dispatch(actionCreators.changeInput(value)),
        add: () => dispatch(actionCreators.add())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddToDo);