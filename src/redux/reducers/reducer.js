import * as actionTypes from '../actions/action-types';
const initialState = {
    todoList : [
        {taskName:'task 1',id:1,checked:false},
        {taskName:'task 2',id:2,checked:true}
    ],
    input : ''
}

export const reducer = (state = initialState, actions) => {
    switch ( actions.type ) {
        case actionTypes.CHANGE_INPUT:
            return {
                ...state,
                input : actions.input
            }
        case actionTypes.ADD:
            const newTask = {
                taskName: state.input.trim(),
                id: Date.now(),
                checked:false
            }
            return {
                ...state,
                todoList : state.todoList.concat(newTask),
                input: ''
            }
        case actionTypes.DELETE:
            return {
                ...state,
                todoList : state.todoList.filter(elem => {
                    return elem.id !== actions.id
                })
            }
        case actionTypes.DONE_UNDONE:
            return {
                ...state,
                todoList: state.todoList.map(task => {
                    if (task.id === actions.id) {
                        task.checked = !task.checked;
                    }
                    return task;
                })
            }
        case actionTypes.EDIT:
            return {
                ...state,
                todoList: state.todoList.map(task => {
                    if (task.id === actions.id) {
                      task.taskName = actions.newValue
                    }
                    return task;
                })
            }
        case actionTypes.MARK_ALL_DONE_UNDONE:
            if(!state.todoList.length) {
                return {
                    ...state
                }
            } else {
                let value = state.todoList.every(elem => {
                    return elem.checked
                })
                if (value) {
                    return {
                        ...state,
                        todoList: state.todoList.map(task => {
                            task.checked = false
                            return task
                        })
                    }
                } else {
                    return {
                        ...state,
                        todoList: state.todoList.map(task => {
                            task.checked = true
                            return task
                        })
                    }
                }
            }
        case actionTypes.CLEAR_DONE:
            return {
                ...state,
                todoList: state.todoList.filter(each => {
                    return !each.checked
                })
            }
        default:
            return state
    }
};