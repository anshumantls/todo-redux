import React , {Component} from 'react';
import { connect } from 'react-redux';
import * as actionCreator from '../redux/actions/todo-action-creator';  

class ToDoList extends Component {
    render () {
        let ToDoList
        if (this.props.list.length) {
            ToDoList  = this.props.list.map(task => {
                return <li key={task.id}>
                            <input type="checkbox" onChange={() => this.props.doneUndone(task.id)} checked={task.checked}/>
                            <input type="text" value={task.taskName} onChange={(event) => this.props.edit(task.id,event)}/>
                            <button className='delete' onClick={() => this.props.delete(task.id)}>Delete</button>
                        </li>;
            });
        }
        return (
            <div>
                {ToDoList}
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        delete: (id) => dispatch(actionCreator.dlt(id)),
        doneUndone: (id) => dispatch(actionCreator.doneUndone(id)),
        edit: (id,event) => dispatch(actionCreator.edit(id,event.target.value))
    }
}

export default connect(null, mapDispatchToProps)(ToDoList);