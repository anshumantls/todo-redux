export const CHANGE_INPUT = 'CHANGE_INPUT';
export const ADD = 'ADD';
export const DELETE = 'DELETE';
export const DONE_UNDONE = 'DONE_UNDONE';
export const EDIT = 'EDIT';
export const MARK_ALL_DONE_UNDONE = 'MARK_ALL_DONE_UNDONE';
export const CLEAR_DONE = 'CLEAR_DONE';
