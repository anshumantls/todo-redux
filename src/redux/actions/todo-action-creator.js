import * as actionTypes from './action-types';

export const changeInput = (value) => {
    return {
        type: actionTypes.CHANGE_INPUT,
        input: value
    }
}

export const add = () => {
    return {
        type: actionTypes.ADD
    }
}

export const dlt = (value) => {
    return {
        type: actionTypes.DELETE,
        id: value
    }
}

export const doneUndone = (value) => {
    return {
        type: actionTypes.DONE_UNDONE,
        id: value
    }
}

export const edit = (id,value) => {
    return {
        type: actionTypes.EDIT,
        id: id,
        newValue: value
    }
}

export const markAllDoneUndone = () => {
    return {
        type: actionTypes.MARK_ALL_DONE_UNDONE
    }
}

export const clearDone = () => {
    return {
        type: actionTypes.CLEAR_DONE
    }
}





