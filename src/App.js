import React, { Component } from 'react';
import { connect } from 'react-redux';
import './App.css';
import * as actionCreators from './redux/actions/todo-action-creator';

//components
import Remaining from './components/Remaining';
import ToDoList from './components/ToDoList';
import AddToDo from './components/AddToDo';
 
class App extends Component  {
  state = {
    filterVariable: 'all'
  }

  render() {
    let propList = '';
    let all = '',remain = '',done='';
    let checked = false;
    if (this.state.filterVariable === 'all') {
      propList = this.props.list;
      all = 'active';
      remain = '';
      done = '';
    }
    else if (this.state.filterVariable === 'remaining') {
      propList = this.props.list.filter(elem => {
        return elem.checked === true
      });
      all = '';
      remain = 'active';
      done = '';
    }
    else if (this.state.filterVariable === 'done') {
      propList = this.props.list.filter(elem => {
        return elem.checked === false
      });
      all = '';
      remain = '';
      done = 'active';
    }
    let remaining = 0;
    if (this.props.list.length > 0) {
      this.props.list.filter(elem => {
        if (elem.checked === false) {
          remaining++;
        }
        return true;
      });
    }

    checked = this.props.list.every(each => {
      return each.checked;
    })

    return (
      <div className="App">
          <h1>todos</h1>
          
          <AddToDo/>
          <div className='features'>
            <input type='checkbox' onChange={this.props.markAllDoneUndone} checked={checked}/><p>Mark All Done/Undone</p>
            <button onClick={this.props.clearDone}>Clear Done</button>
          </div>

          <div>
            <ul>
              <ToDoList list={propList}/>
            </ul>
          </div>

          <div className='filter'>
            <h3 className={all} onClick={() => this.setState({filterVariable:'all'})}>Show All</h3>
            <h3 className={remain} onClick={() => this.setState({filterVariable:'remaining'})}>Show Completed</h3>
            <h3 className={done} onClick={() => this.setState({filterVariable:'done'})}>Show Remaining</h3>
          </div>
          <Remaining value={remaining}/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state)
  return {
    list : state.reducer.todoList
  }
}

const mapDispatchToProps = dispatch => {
  return {
    markAllDoneUndone : () => dispatch(actionCreators.markAllDoneUndone()),
    clearDone: () => dispatch(actionCreators.clearDone())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
